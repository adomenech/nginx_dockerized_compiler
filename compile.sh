#!/bin/bash
set -e

docker build -t nginx_compiler .
docker run --name nginxc -v $(pwd)/output:/root/rpmbuild/RPMS nginx_compiler
docker rm -f nginxc
#docker rmi -f nginx_compiler
sudo chown -R ${USER}:${USER} $(pwd)/output

exit 0