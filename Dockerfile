FROM centos:centos7
MAINTAINER Albert Domenech (adomenech@cnmc.es)

ENV \
	NGINX_VERSION=1.9.9 \
	PGSPEED_VERSION=1.9.32.10 \
	NAXSI_VERSION=0.54

RUN yum update -y && \
	yum install -y net-tools deltarpm \
					wget curl-devel \
					zlib-devel \
					openssl-devel \
					pcre pcre-devel \
					make gcc gcc-c++ \ 
					rpm-build && \
					useradd -m builder

WORKDIR /root

RUN	mkdir rpmbuild && \
	mkdir rpmbuild/SOURCES && \
	mkdir rpmbuild/SPECS && \
	mkdir rpmbuild/BUILD && \
	mkdir rpmbuild/RPMS && \
	mkdir rpmbuild/SRPMS

ADD data/nginx.spec rpmbuild/SPECS
ADD data/logrotate rpmbuild/SOURCES
ADD data/nginx.conf rpmbuild/SOURCES
ADD data/nginx.init rpmbuild/SOURCES
ADD data/nginx.service rpmbuild/SOURCES
ADD data/nginx.spec rpmbuild/SOURCES
ADD data/nginx.suse.init rpmbuild/SOURCES
ADD data/nginx.suse.logrotate rpmbuild/SOURCES
ADD data/nginx.sysconf rpmbuild/SOURCES
ADD data/nginx.upgrade.sh rpmbuild/SOURCES
ADD data/nginx.vh.default.conf rpmbuild/SOURCES
ADD data/nginx.vh.example_ssl.conf rpmbuild/SOURCES
ADD data/naxsi_core.rules rpmbuild/SOURCES
ADD data/naxsi.rules rpmbuild/SOURCES

RUN mkdir /output && \
	cd rpmbuild/SOURCES && \
	wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
	wget https://github.com/yaoweibin/ngx_http_substitutions_filter_module/archive/v0.6.4.tar.gz && \
	wget https://github.com/pagespeed/ngx_pagespeed/archive/v${PGSPEED_VERSION}-beta.tar.gz && \
	tar xvf v${PGSPEED_VERSION}-beta.tar.gz && \
	rm v${PGSPEED_VERSION}-beta.tar.gz && \
	cd ngx_pagespeed-${PGSPEED_VERSION}-beta && \
	wget https://dl.google.com/dl/page-speed/psol/${PGSPEED_VERSION}.tar.gz && \
	tar xvf ${PGSPEED_VERSION}.tar.gz && \
	rm ${PGSPEED_VERSION}.tar.gz && \
	cd .. && \
	tar -zcvf ngx_pagespeed-release-${PGSPEED_VERSION}-beta.tar.gz ngx_pagespeed-${PGSPEED_VERSION}-beta && \
	rm -r ngx_pagespeed-${PGSPEED_VERSION}-beta && \
	wget https://github.com/nbs-system/naxsi/archive/${NAXSI_VERSION}.tar.gz

VOLUME /root/rpmbuild/RPMS

CMD ["/usr/bin/rpmbuild", "-ba", "/root/rpmbuild/SPECS/nginx.spec"]
