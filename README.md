## What?

This is a dockerfile to create a container for building Centos7 rpm package from the last nginx released versión, adding some extra modules on the way.

## Ok, but exactly What?

Nginx: 1.9.9

Nginx Google Page Speed plugin: 1.9.32.10

Nginx http substitutions filter: 0.6.4 

Nginx Naxsi plugin: 0.34

## Fine, what I need?

Just Docker.

## The lazy how to:

```Bash
./compile.sh
```

## How in detail:

The detailed way to generate the rpm is:

1 - Generating the container image: downloads all the sources needed

```Bash
docker build -t nginx_compiler .
```

2 - Executing the container image: compilation and rpm generation

```Bash
docker run --name nginxc -v $(pwd)/output:/root/rpmbuild/RPMS nginx_compiler

```

3 - Clean the dust

```Bash
docker rm -f nginxc
docker rmi -f nginx_compiler

```

Enjoy

**ado**
